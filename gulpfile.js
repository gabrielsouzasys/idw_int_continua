var browserify = require('./node_modules/browserify');
var gulp = require('./node_modules/gulp');
var babelify = require('babelify');

var source = require('./node_modules/vinyl-source-stream');

gulp.task('browserify', function(){
    return browserify('./src/app.js')
    .transform(babelify.configure()) 
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./public/'));

});

gulp.task('watch', function(){
    gulp.watch('./src/*.js',gulp.series('browserify'));
});